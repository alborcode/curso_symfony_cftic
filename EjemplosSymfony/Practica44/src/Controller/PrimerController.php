<?php

namespace App\Controller;

use App\Entity\Hospital;
use App\Entity\Plantilla;
use App\Repository\DeptRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PrimerController extends AbstractController
{
    #[Route('/', name: 'hospitales')]
    public function hospitales(Request $request, EntityManagerInterface $em)
    { 
        // Recupero en Array todos los hospitales de la BBDD
        $datos = $em->getRepository(Hospital::class)->findAll();  
        return $this->render('Hospitales.html.twig', ['datosHospitales' => $datos]);
    }

    #[Route('/mostrar', name: 'ver_datos')]
    public function ver_datos(Request $request, EntityManagerInterface $em)
    { 
        // Se recoge con get el parametro que hemos mandado
        $datoget = $request->query->get('id');
        // Busco por el Id que me llega
        $datos = $em->getRepository(Plantilla::class)->findByHospitalCod($datoget);
        // Si no existe datos es que no hay plantilla para ese codigo de hospital
        if(!$datos) {
            return $this->render('VerDatos.html.twig', [
                'mensaje' => 'No existe Codigo de Hospital'
            ]);
        }else{
            return $this->render('VerDatos.html.twig', [
                'datosPlantilla' => $datos,
                'codigoHospital' => $datoget
            ]);
        }
    }

}