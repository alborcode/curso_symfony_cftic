<?php

namespace App\Controller;

use App\Entity\Emp;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
// Use necesario para usar las funciones de paginacion
use Knp\Component\Pager\PaginatorInterface;

class PaginaController extends AbstractController
{
    #[Route('/Paginacion', name: 'paginar')]
    public function paginar(Request $request, PaginatorInterface $paginator,EntityManagerInterface $em)
    {
        // Recupero todos los registros de la tabla Emp
        $query = $em->getRepository(Emp::class)->findAll();
        
        // Paginar los resultados de la consulta
        $datosPaginados = $paginator->paginate(
            $query, // Consulta que quiero paginar
            $request->query->getInt('page', 1), // Definir el parámetro de la página recogida por GET
            5 // Número de elementos por página
        );

        return $this->render('Index.html.twig', 
        [
            'datosP' => $datosPaginados
        ]);
    }
}