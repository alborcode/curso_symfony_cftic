<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpFoundation\Request;
use App\Entity\Emp;
class PrimerController extends AbstractController
{
    #[Route('/lecturaget', name: 'inicio')]
    public function inicio(Request $request, EntityManagerInterface $em)
    {
        // Para las API se recomienda usar DQL porque no da errores el Array asociativo del JSON
        $query = $em->createQuery(
            'SELECT e.apellido as apellido, e.oficio as oficio, e.salario as salario
                FROM App\Entity\Emp e'
            );
        $datos = $query->getArrayResult();
        return new JsonResponse($datos);
    }
}