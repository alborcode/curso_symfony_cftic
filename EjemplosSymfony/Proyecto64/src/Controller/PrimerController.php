<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PrimerController extends AbstractController
{
    // En la ruta damos el locale para que recoga el inicio correspondiente a cada idioma
    #[Route('/{_locale}/inicio', name: 'app_emp_index',)]
    public function index(Request $request, TranslatorInterface $translator)
    {
        $locale = $request->getLocale();
        // Podríamos recuperarlo por código y enviarlo a la vista
        //$dato = $translator->trans('mensajenombre');
        return $this->render('Inicio.html.twig', 
            ['idioma' => $locale]
        );
    }
}