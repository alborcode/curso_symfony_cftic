<?php

namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
class PrimerController extends AbstractController
{
    #[Route('/consumoapi', name: 'inicio')]
    public function inicio(Request $request)
    {
        $datos = file_get_contents("http://localhost:8000/lecturaget");
        $cotilleo = json_decode($datos, true);
        
        return $this->render('index.html.twig', [
            'datosPrograma' => $cotilleo
        ]);
    }

    #[Route('/consumoapiparm', name: 'inicio2')]
    public function inicio2(Request $request)
    {
        $datos = file_get_contents("http://localhost:8000/lecturaget/1");
        $cotilleo = json_decode($datos, true);
        
        return $this->render('indexparametro.html.twig', [
            'datosPrograma' => $cotilleo
        ]);
    }
}