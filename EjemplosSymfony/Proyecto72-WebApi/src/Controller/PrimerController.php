<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class PrimerController extends AbstractController
{
    // No pongo methods dado que por defecto es get
    #[Route('/lecturaget', name: 'inicio')]
    public function inicio()
    {
        $datos = [
            [
                'nombre' => 'Benito',
                'apellido' => 'Floro'
            ],
            [
                'nombre' => 'Maria',
                'apellido' => 'Patiño'
            ],
            [
                'nombre' => 'Carlos',
                'apellido' => 'Lozano'
            ],
            [
                'nombre' => 'Kiko',
                'apellido' => 'Matamoros'
            ],
            [
                'nombre' => 'Rafa',
                'apellido' => 'Mora'
            ],
            [
                'nombre' => 'Kiko',
                'apellido' => 'Matamoros'
            ],
            [
                'nombre' => 'Alonso',
                'apellido' => 'Caparros'
            ],
            [
                'nombre' => 'Isa',
                'apellido' => 'Pantoja'
            ],
        ];
        // El retorno es en formato Json dado que es un API
        return new JsonResponse($datos);
    }

    // Metodo pasando un parametro
    #[Route('/lecturaget/{id}', name: 'inicio2')]
    public function inicio2($id)
    {
        $datos = [
            [
                'nombre' => 'Benito',
                'apellido' => 'Floro'
            ],
            [
                'nombre' => 'Maria',
                'apellido' => 'Patiño'
            ],
            [
                'nombre' => 'Carlos',
                'apellido' => 'Lozano'
            ],
            [
                'nombre' => 'Kiko',
                'apellido' => 'Matamoros'
            ],
            [
                'nombre' => 'Rafa',
                'apellido' => 'Mora'
            ],
            [
                'nombre' => 'Kiko',
                'apellido' => 'Matamoros'
            ],
            [
                'nombre' => 'Alonso',
                'apellido' => 'Caparros'
            ],
            [
                'nombre' => 'Isa',
                'apellido' => 'Pantoja'
            ],
        ];
        
        return new JsonResponse($datos[$id]);
    }
}
