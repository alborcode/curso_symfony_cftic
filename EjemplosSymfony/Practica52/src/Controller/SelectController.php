<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Emp;
use Doctrine\ORM\EntityManagerInterface;

class SelectController extends AbstractController
{

    // Mostrar Datos Departamentos
    #[Route('/buscar', name: 'formulario')]
    public function formulario(Request $request, EntityManagerInterface $em)
    { 
        return $this->render('empleados.html.twig');
    }

    // Mostrar Datos Departamentos
    #[Route('/busquedaempleados', name: 'buscarEmpleados')]
    public function buscarEmpleados(Request $request, EntityManagerInterface $em)
    {
        // Se recoge con get el parametro que hemos mandado en la caja de texto request al ser post, query si es get
        $datobusqueda = $request->request->get('txtApellido');
        dump($datobusqueda);
        
        $query = $em->createQuery('SELECT e FROM App\Entity\Emp e WHERE e.apellido like :parametro');
        // Concateno la variable a buscar y el % del Like
        $query->setParameter('parametro', $datobusqueda.'%');
        dump($query);

        $datos = $query->getResult();
        dump($datos);

        return $this->render('empleados.html.twig', 
        [
            'datosEmpleados' => $datos,
        ]); 

    }
    
}