<?php

namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
class PrimerController extends AbstractController
{
    #[Route('/consumoapi', name: 'inicio')]
    public function inicio(Request $request)
    {
        $datos = file_get_contents("http://localhost:8000/lecturaget");
        $datosEmpleados = json_decode($datos, true);
        return $this->render('index.html.twig', [
            'datosServicio' => $datosEmpleados
        ]);
    }
}