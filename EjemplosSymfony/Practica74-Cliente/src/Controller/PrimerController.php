<?php

namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
class PrimerController extends AbstractController
{
    #[Route('/nombre', name: 'funcionNombre')]
    public function funcionNombre(Request $request)
    {
        $datos = file_get_contents("https://apiazuresymfony20220906102006.azurewebsites.net/api/values");
        $datosNombre = json_decode($datos, true);
        dump($datosNombre);

        return $this->render('indexnombre.html.twig', [
            'datos' => $datosNombre
        ]);
    }

    #[Route('/edad/{edad}', name: 'funcionEdad')]
    public function funcionEdad(Request $request, int $edad)
    {
        $url = "https://apiazuresymfony20220906102006.azurewebsites.net/api/values/".$edad;
        $datos = file_get_contents($url);
        //$datosEdad = json_decode($datos, true);
        dump($datos);
        
        return $this->render('indexedad.html.twig', [
            'datos' => $datos
        ]);
    }

    #[Route('/nombreapellido/{nombre}/{apellido}', name: 'funcionNombreApellido')]
    public function funcionNombreApellido(Request $request, string $nombre, string $apellido)
    {
        $url = "https://apiazuresymfony20220906102006.azurewebsites.net/api/values/".$nombre."/".$apellido;
        $datos = file_get_contents($url);
        //$datosNombreApellido = json_decode($datos, true);
        dump($datos);
        
        return $this->render('indexnombreapellido.html.twig', [
            'datos' => $datos
        ]);
    }

    #[Route('/ciudad/{nombreciudad}', name: 'funcionCiudad')]
    public function funcionCiudad(Request $request, string $nombreciudad)
    {
        $url = "https://apiazuresymfony20220906102006.azurewebsites.net/api/values/ciudad/".$nombreciudad;
        $datos = file_get_contents($url);
        //$datosNombreCiudad = json_decode($datos, true);
        dump($datos);
        
        return $this->render('indexnombreciudad.html.twig', [
            'datos' => $datos
        ]);
    }

}