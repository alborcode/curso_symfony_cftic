<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Hospital;
use App\Entity\Doctor;
use App\Entity\Plantilla;
use Doctrine\ORM\EntityManagerInterface;

class SelectController extends AbstractController
{
    #[Route('/Index', name: 'inicio')]
    public function inicio()
    {
        return $this->render('Index.html.twig');
    }

    // Mostrar Datos de los Doctores
    #[Route('/Doctores', name: 'recuperaDoctores')]
    public function recuperaDoctores(Request $request, EntityManagerInterface $em)
    {
        // Recupero en Array todos los doctores de la BBDD
        $datosdoctores = $em->getRepository(Doctor::class)->findAll();  
        
        return $this->render('doctores.html.twig', [
            'datosDoctores' => $datosdoctores
        ]);
    }

    // Mostrar Datos de los Hospitales
    #[Route('/Hospitales', name: 'recuperaHospitales')]
    public function recuperaHospitales(Request $request, EntityManagerInterface $em)
    {
        // Recupero en Array todos los hospitales de la BBDD
        $datoshospitales = $em->getRepository(Hospital::class)->findAll();  
        
        return $this->render('hospitales.html.twig', [
            'datosHospitales' => $datoshospitales
        ]);
    }

    // Mostrar Datos de las Plantillas
    #[Route('/Plantillas', name: 'recuperaPlantillas')]
    public function recuperaPlantillas(Request $request, EntityManagerInterface $em)
     {
        // Recupero en Array todas las plantillas de la BBDD
        $datosplantillas = $em->getRepository(Plantilla::class)->findAll();  
         
        return $this->render('plantillas.html.twig', [
             'datosPlantillas' => $datosplantillas
        ]);
     }
        
}