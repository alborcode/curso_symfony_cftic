<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Doctor
 *
 * @ORM\Table(name="doctor")
 * @ORM\Entity(repositoryClass="App\Repository\DoctorRepository")
 */
class Doctor
{
    /**
     * @var int
     *
     * @ORM\Column(name="doctor_cod", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $doctorCod;

    /**
     * @var string
     *
     * @ORM\Column(name="hospital_cod", type="decimal", precision=11, scale=0, nullable=false)
     */
    private $hospitalCod;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=50, nullable=false)
     */
    private $apellido;

    /**
     * @var string
     *
     * @ORM\Column(name="especialidad", type="string", length=20, nullable=false)
     */
    private $especialidad;

    /**
     * @var float
     *
     * @ORM\Column(name="salario", type="float", precision=10, scale=0, nullable=false)
     */
    private $salario;

    public function getDoctorCod(): ?int
    {
        return $this->doctorCod;
    }

    public function getHospitalCod(): ?string
    {
        return $this->hospitalCod;
    }

    public function setHospitalCod(string $hospitalCod): self
    {
        $this->hospitalCod = $hospitalCod;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getEspecialidad(): ?string
    {
        return $this->especialidad;
    }

    public function setEspecialidad(string $especialidad): self
    {
        $this->especialidad = $especialidad;

        return $this;
    }

    public function getSalario(): ?float
    {
        return $this->salario;
    }

    public function setSalario(float $salario): self
    {
        $this->salario = $salario;

        return $this;
    }


}
