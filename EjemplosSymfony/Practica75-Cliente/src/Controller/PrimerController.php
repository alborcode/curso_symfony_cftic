<?php

namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
class PrimerController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(Request $request)
    {

        return $this->render('index.html.twig', []);
    }

    #[Route('/buscar', name: 'buscarEmpleado')]
    public function buscarEmpleado(Request $request)
    {
        // Recogemos datos de formulario con Get dado que es una busqueda
        // $id = $request->request->get('txtId');
        $id = $request->query->get('txtId');
        dump($id);
        $url = "https://apiejemplo2-220220907114510.azurewebsites.net/api/empleados/".$id;
        $datos = file_get_contents($url);
        $datosId = json_decode($datos, true);
        dump($datosId);

        return $this->render('index.html.twig', [
            'datosId' => $datosId
        ]);
    }

    #[Route('/buscaroficio', name: 'buscarEmpleadoOficio')]
    public function buscarEmpleadoOficio(Request $request)
    {
        // Recogemos datos de formulario con Get dado que es una busqueda
        // $id = $request->request->get('txtId');
        $oficio = $request->query->get('txtOficio');
        dump($oficio);
        $url = "https://apiejemplo2-220220907114510.azurewebsites.net/api/empleados/devolverOficio/".$oficio;
        $datos = file_get_contents($url);
        $datosOficio = json_decode($datos, true);
        dump($datosOficio);

        return $this->render('index.html.twig', [
            'datosEmpleadosOficio' => $datosOficio
        ]);
    }


    // Ruta para Detalle de mostrar Todos los Empleados
    #[Route('/mostrar', name: 'recuperarEmpleados')]
    public function recuperarEmpleados(Request $request)
    { 
        // Recupero todos los empleados de la API
        $datos = file_get_contents("https://apiejemplo2-220220907114510.azurewebsites.net/api/empleados");
        $datosEmpleados = json_decode($datos, true);
        dump($datosEmpleados);

        return $this->render('index.html.twig', [
            'datosEmpleados' => $datosEmpleados,
        ]);
    }

}