<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Emp;
use App\Repository\EmpRepository;


class PrimerController extends AbstractController
{
    #[Route('/Index', name: 'oficios')]
    public function oficios(Request $request, EntityManagerInterface $em)
    { 
        //Cuando trabajamos con DQL, no trabajamos con tablas y campos de las tablas sino con entidades y con propiedades de las entidades.
        // Obligatorio el alias (u) para poder acceder a las propiedades
        // Recupero con Distinct los oficios (la variable oficio debe ir como el private en minusculas)
        $query = $em->createQuery('SELECT DISTINCT e.oficio FROM App\Entity\Emp e');
        dump($query);
        $datos = $query->getResult();
        dump($datos);
        return $this->render('EmpleadosOficio.html.twig', [
            'datosOficios' => $datos
        ]);
    }

    #[Route('/mostrar', name: 'recuperarEmpleados')]
    public function recuperarEmpleados(Request $request, EntityManagerInterface $em)
    { 
        // Se recoge con get el parametro que hemos mandado
        $datoget = $request->query->get('oficio');
        dump($datoget);
        // Busco por el Id que me llega
        $datos = $em->getRepository(Emp::class)->findByOficio($datoget);
        // Si no existe datos es que no hay plantilla para ese codigo de hospital
        return $this->render('VerDatos.html.twig', [
            'datosEmpleados' => $datos,
            'oficio' => $datoget
        ]);
    }

}