<?php

namespace App\Controller;

//use App\Entity\Dept;
//use App\Repository\DeptRepository;
//use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PrimerController extends AbstractController
{
    // Pagina inicial con formulario de operaciones
    #[Route('/inicio', name: 'verPagina')]
    public function verPagina(Request $request)
    { 
        return $this->render('Inicio.html.twig');
    }
    
    // Ruta de Operaciones
    #[Route('/multiplesDatos', name: 'multiples')]
    public function multiples(Request $request)
    { 
        $dato= $request->request->get('operacion');
        $num1 = $request->request->get('numero1');
        $num2 = $request->request->get('numero2');
        dump("gfjhgghjghj");
        // Controlo el value de operacion (ambos botone tienen mismo name)
        if ($dato=='sumarNumeros'){
            $resultado = $num1 + $num2;
            $operacion = "SUMA";
        }
        else{
            $resultado = $num1 * $num2;
            $operacion = "MULTIPLICACIÓN";
        }
        // Envio a pagina Inicio enviando operacion y resultado
        return $this->render('Inicio.html.twig', 
        [
            'operacion' => $operacion,
            'resultado' => $resultado
        ]);
    }

}
