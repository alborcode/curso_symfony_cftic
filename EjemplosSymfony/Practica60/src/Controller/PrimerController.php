<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Emp;
use App\Repository\EmpRepository;

// ESPACIOS DE NOMBRES DE LA BIBLIOTECA DOMPDF
use Dompdf\Dompdf;
use Dompdf\Options;


class PrimerController extends AbstractController
{
    #[Route('/Index', name: 'empleados')]
    public function empleados(Request $request, EntityManagerInterface $em)
    { 
        // Recupero en Array todos los Empleados de la BBDD
        $datosempleados = $em->getRepository(Emp::class)->findAll();  
        return $this->render('Empleados.html.twig', [
            'datosEmpleados' => $datosempleados
        ]);
    }

    #[Route('/mostrar', name: 'informeEmpleado')]
    public function informeEmpleado(Request $request, EntityManagerInterface $em)
    { 
        // Se recoge con get el parametro que hemos mandado con el codigo de empleado
        $datoget = $request->query->get('codigo');
        dump($datoget);
        // Busco por codigo de Empleado
        $datos = $em->getRepository(Emp::class)->find($datoget);
        // Recuperamos el Apellido del registro leido
        //$apellido = $datos->getApellido();

        // Preparamos la página HTML para generar PDF generano un objeto renderview
        $html = $this->renderView('InformePDF.html.twig', 
        [
            'datosInforme'  => $datos,
        ]);

        // Existen muchas configuraciones para Dompdf. Incluimos una de las muchas que tiene por ejemplo asignar el tipo de letra
        $pdfOptions = new Options();
        // Tipo de Letra con la que escribir
        $pdfOptions->set('defaultFont', 'Arial');
        // Crea una instancia de Dompdf con nuestras opciones
        $dompdf = new Dompdf($pdfOptions);

        // Ahora se carga la página HTML en Dompdf        
        $html .= '<link type="text/css" href="/public/bootstrap/css/bootstrap.min.css" rel="stylesheet" />';
        $dompdf->loadHtml($html);

        // También podemos de forma opcional indicar el tamaño del papel y la orientación
        $dompdf->setPaper('A4', 'portrait');
 
        // Renderiza el HTML como PDF
        $dompdf->render();

        // Podemos generar el pdf y visualizarlo en el navegador si modificamos la propiedad Attachment al valor false.
            $dompdf->stream("Informe.pdf", [
            "Attachment" => false
        ]);
    }


}