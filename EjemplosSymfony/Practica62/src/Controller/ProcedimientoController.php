<?php

namespace App\Controller;

use App\Entity\Enfermo;
use App\Repository\EnfermoRepository;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProcedimientoController extends AbstractController
{
    // Pagina inicial con formulario de Numero de SS
    #[Route('/inicio', name: 'Inicio')]
    public function Inicio(Request $request, EntityManagerInterface $em)
    { 
        return $this->render('Inicio.html.twig');
    }

    #[Route('/ss', name: 'FormularioSS')]
    public function FormularioSS(Request $request, EntityManagerInterface $em)
    { 
        // Recogemos el numero de la SS insertado en el formulario
        // Recogemos el numero de la SS del formulario
        $numeross = $request->request->get('txtNumeroSS');

        // Buscamos con el Numero de la Seguridad Social para ver si existe en Tabla Enfermos
        $datosenfermo = $em->getRepository(Enfermo::class)->find($numeross);
        
        // Si no existe ese numero
        if(!$datosenfermo) {
            // Recupero Identificador de sesion (Token) del usuario de la peticion
            $session = $request->getSession();
            // Guardo variable Numero de Seguridad Social en Session
            $session->set('numss', $numeross);
            // Mandamos a pagina de Alta sin mandar variables dado que estan en sesion (globales)
            return $this->render('AltaEnfermo.html.twig');
        // Si existe enfermo vuelvo a mandar a pagina de Inicio mandando mensaje    
        }else{
            $mensaje = 'ENFERMO YA INGRESADO';
            return $this->render('Inicio.html.twig', 
            [
                'mensaje' => $mensaje
            ]);
        }
    }

    #[Route('/altabbdd', name: 'altaEnfermo')]
    public function altaEnfermo(Request $request, EntityManagerInterface $em)
    {
        // Recogemos datos de formulario
        $inscripcion = $request->request->get('txtInscripcion');
        $apellido = $request->request->get('txtApellido');
        $direccion = $request->request->get('txtDireccion');
        $fechanacimiento = $request->request->get('txtFechanacimiento');
        $sexo = $request->request->get('txtSexo');
        
        // Recupera variables Session
        $ss = $request->getSession()->get('numss');
        dump($ss);

        // Obtengo la conexion a la que me he enlazado
        $connection = $em->getConnection();
        // CREATE PROCEDURE InsertEnfermo (IN Ape VARCHAR(40), IN Dir VARCHAR(50), IN Fec DATE, IN Ins INT, IN ss INT, IN sex VARCHAR(1))
        // INSERT INTO enfermo (APELLIDO, DIRECCION, FECHA_NAC, INSCRIPCION, NSS, SEXO)
        // VALUES (Ape, Dir, Fec, Ins, ss, sex);
        // Sobre esa conexion ejecuto el procedimiento con los parametros correspondientes separados por comas
        // Se podria lanzar Select tal cual en vez de Call a procedimiento
        $statement = $connection->prepare("CALL insertEnfermo(:apellido, :direccion, :fechanamiento, :inscripcion, :ss, :sexo)");
        
        // Damos valor a los parametros
        $statement->bindValue('apellido', $apellido);
        $statement->bindValue('direccion', $direccion);
        $statement->bindValue('fechanamiento', $fechanacimiento);
        $statement->bindValue('inscripcion', $inscripcion);
        $statement->bindValue('ss', $ss);
        $statement->bindValue('sexo', $sexo);

        // Ejecuto el procedimiento con el numero de registros afectados
        $resultado= $statement->executeStatement();
        
        // Envio a la pagina mandando el numero de registros afectados
        return $this->render('Altaenfermo.html.twig', 
            [
                'afectados' => $resultado,
            ]);
    }

}