<?php

namespace App\Controller;

use App\Entity\Dept;
use App\Repository\DeptRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PrimerController extends AbstractController
{
    // Pagina inicial con formulario de operaciones
    #[Route('/inicio', name: 'formulario')]
    public function formulario(Request $request)
    { 
        return $this->render('departamentos.html.twig');
    }
    
    // Ruta de Operaciones
    #[Route('/operacionesDept', name: 'operacionesDept')]
    public function operacionesDept(Request $request, EntityManagerInterface $em)
    { 

        $boton= $request->request->get('operacion');

        // Controlo el value de operacion (todos los botones tienen mismo name)

        // Si es insertar
        if ($boton=='insertar'){
            //$numerodepartamento= $request->request->get('txtNumDep');
            $nombre= $request->request->get('txtNombre');
            $localidad= $request->request->get('txtLocalidad');
            
            $departamento = new Dept();
            $departamento->setDnombre($nombre);
            $departamento->setLoc($localidad);
                    
            $em->persist($departamento);
            $em->flush();
            return $this->render('departamentos.html.twig', 
            [
                'mensaje' => 'Departamento correctamente dado de Alta'
            ]);
        }

        // Si es modificar
        if ($boton=='modificar'){
            $numerodepartamento= $request->request->get('txtNumDep');
            $nombre= $request->request->get('txtNombre');
            $localidad= $request->request->get('txtLocalidad');
                    
            $departamento = $em->getRepository(Dept::class)->find($numerodepartamento);
            
            if(!$departamento) {
                $mensaje = 'Departamento no existe';
                return $this->render('departamentos.html.twig', 
                [
                    'mensaje' => $mensaje
                ]);
            }else{
                $departamento->setDnombre($nombre);
                $departamento->setLoc($localidad);
                
                $em->persist($departamento);
                $em->flush();
                $mensaje = 'Departamento Modificado correctamente';
                return $this->render('departamentos.html.twig', 
                [
                    'mensaje' => $mensaje
                ]);
            }
        }

        // Si es eliminar
        if ($boton=='eliminar'){   
            $numerodepartamento= $request->request->get('txtNumDep');                   
            $departamento = $em->getRepository(Dept::class)->find($numerodepartamento);
            
            if(!$departamento) {
                $mensaje = 'Departamento no existe';
                return $this->render('departamentos.html.twig', 
                [
                    'mensaje' => $mensaje
                ]);
            }else{
                $em->remove($departamento);
                $em->flush();
                $mensaje = 'Departamento Borrado correctamente';
                return $this->render('departamentos.html.twig', 
                [
                    'mensaje' => $mensaje
                ]);
            }

        }
    }

}