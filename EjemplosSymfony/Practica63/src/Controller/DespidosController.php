<?php

namespace App\Controller;

use App\Entity\Enfermo;
use App\Repository\EnfermoRepository;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Emp;
use App\Repository\EmpRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DespidosController extends AbstractController
{
    // Pagina inicial con formulario de Numero de SS
    #[Route('/inicio', name: 'Empleados')]
    public function Empleados(Request $request, EntityManagerInterface $em)
    { 
        // Recupero en Array todos los Empleados de la BBDD
        $datosempleados = $em->getRepository(Emp::class)->findAll();  
        dump($datosempleados);
        return $this->render('Empleados.html.twig', [
            'datosEmpleados' => $datosempleados
        ]);
    }

    // Pagina en el Path al dar a Almacenar un Empleado
    #[Route('/almacenar', name: 'almacenarEmpleado')]
    public function almacenarEmpleado(Request $request, EntityManagerInterface $em)
    {

        // Se recoge con get el parametro del codigo de empleado del enlace pulsado
        $empleado = $request->query->get('codempleado');
        dump('$empleado:'.$empleado);

        // Recupero Identificador de sesion (Token) del usuario de la peticion
        $session = $request->getSession();
        $existe = $session->has('arrayempleados');
        // Pregunto si existe ArrayEmpleados en Session
        if($existe){
            // Recupero el Array de Empleados de Sesion
            $arrayempleados = $request->getSession()->get('arrayempleados');
            // Añado el empleado al array de Empleados
            array_push($arrayempleados, $empleado);
            // Actualizo el array de Empleados de la Sesion
            $session->set('arrayempleados', $arrayempleados);
        }else{
            // Creo Array empleados vacio
            $arrayempleados = [];
            // Guardo en Array el empleado
            $arrayempleados[0] = $empleado;
            // Guardo Array Empleados en Session
            $session->set('arrayempleados', $arrayempleados);
        }
        
        // Recupera variable Session arrayempleados guardada
        $arrayempleados = $request->getSession()->get('arrayempleados');
        // Guardo longitud del array de empleados
        $longitud = sizeof($arrayempleados);
        // Guardamos en la variable numempleados la longitud del array
        $session->set('numempleados', $longitud);
        // Construimos mensaje
        $mensaje = 'Numero de empleados almacenados '.$longitud;

        // Recupero en Array todos los Empleados de la BBDD para enviarlos al formulario
        $datosempleados = $em->getRepository(Emp::class)->findAll();  
        // Vuelvo a mandar a pagina Inicial
            return $this->render('Empleados.html.twig', [
                'datosEmpleados'    => $datosempleados,
                'mensaje'           => $mensaje 
            ]);
    }

    // Pagina en el Path al dar a Almacenar un Empleado
    #[Route('/resumen', name: 'resumenEmpleados')]
    public function resumenEmpleados(Request $request, EntityManagerInterface $em)
    {
        // Recupera variable Session arrayempleados
        $arrayempleados = $request->getSession()->get('arrayempleados');
        dump($arrayempleados);
        
        $sumasalarial = 0;
        $longitud = sizeof($arrayempleados);
        dump('$longitud:'.$longitud);

        // Creo Array datos de empleados vacio de tipo Emp
        $datosempleados = [];
        // Recorro el Array de Empleados para sumar el Salario y sacar Datos
        for ($i = 0; $i < $longitud; $i++) {
            // Accedo por el ID al salario del empleado       
            $id = $arrayempleados[$i];
            $empleado = $em->getRepository(Emp::class)->find($id);
            $datosempleados[$i] = $empleado;
            // Recuperamos Salario del Empleado
            $salario = $empleado->getSalario();
            dump('$salario:'.$salario);
            // Sumo la iteraccion del salario de ese empleado
            $sumasalarial = $sumasalarial + $salario;
        }
      
        $mensaje = 'Suma Salarial de Empleados Almacenados '.$sumasalarial;
        dump('$mensaje:'.$mensaje);
        
        // Mando a Pagina Resumen
            return $this->render('ResumenEmpleados.html.twig', [
                'datosempleados'    => $datosempleados,
                'mensaje'           => $mensaje 
            ]);
    }

    // Enlace de Quitar Sesion
    #[Route('/eliminar', name: 'quitarSesion')]
    public function quitarSesion(Request $request, EntityManagerInterface $em)
    {
        $empleadoaborrar = $request->query->get('empleadoaborrar');
        dump('$empleadoaborrar:'.$empleadoaborrar);
        // Recupera variable Session arrayempleados
        $arrayempleados = $request->getSession()->get('arrayempleados');
        dump($arrayempleados);
        
        $longitud = sizeof($arrayempleados);
        dump('$longitud:'.$longitud);

        // Recorro el Array de Empleados para eliminar de la sesion el ID empleado que se envia a quitar
        for ($i = 0; $i < $longitud; $i++) {
            if ($arrayempleados[$i] = $empleadoaborrar){
                // Elimino ese Empleado del Array de Sesion de Empleados
                unset($arrayempleados[$i]);
            }
        }

        // Modificamos el Array de Empleados de Sesion con el Array de Empleados Mopdificado
        $session->set('arrayempleados', $arrayempleados);
        $mensaje = 'Eliminado el Empleado '.$empleadoaborrar;
        dump('$mensaje:'.$mensaje);

        // Creo Array datos de empleados vacio de tipo Emp
        $datosempleados = [];
        $nuevalongitud = sizeof($arrayempleados);
        // Recorro el Array Actualizado de Empleados de Sesion para ir guardando los datos de Empleado a enviar
        for ($i = 0; $i < $nuevalongitud; $i++) {
            // Accedo por el ID a los datos del empleado y guardo en datos a enviar
            $id = $arrayempleados[$i];
            $empleado = $em->getRepository(Emp::class)->find($id);
            $datosempleados[$i] = $empleado;
        }
        
        // Mando a Pagina Resumen
            return $this->render('ResumenEmpleados.html.twig', [
                'datosempleados'    => $datosempleados,
                'mensaje'           => $mensaje 
            ]);
    }

}