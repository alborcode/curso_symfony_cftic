<?php

namespace App\Controller;

use App\Entity\Doctor;
use App\Repository\DoctorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Id;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProcedimientoController extends AbstractController
{
    #[Route('/inicio', name: 'Formulario')]
    public function Formulario(Request $request, EntityManagerInterface $em)
    { 
        return $this->render('Inicio.html.twig');
    }

    #[Route('/alta', name: 'altaDoctor')]
    public function eliminarDepart(Request $request, EntityManagerInterface $em)
    {
        // Recogemos datos de formulario
        $hospitalcod = $request->request->get('txtHospitalCod');
        $apellido = $request->request->get('txtApellido');
        $especialidad = $request->request->get('txtEspecialidad');
        $salario = $request->request->get('txtSalario');
        // Obtengo la conexion a la que me he enlazado
        $connection = $em->getConnection();
        // CREATE PROCEDURE insertarDoctor (in Ape varchar(50), in Esp varchar(40), in Hcod int, in Sal int)
        // INSERT INTO doctor (apellido, especialidad, hospital_cod, salario) VALUES (Ape, Esp, Hcod, Sal);
        // Sobre esa conexion ejecuto el procedimiento con los parametros correspondientes separados por comas
        $statement = $connection->prepare("CALL insertarDoctor(:apellido, :especialidad, :hospitalcod, :salario)");
        // Damos valor a los parametros
        $statement->bindValue('hospitalcod', $hospitalcod);
        $statement->bindValue('apellido', $apellido);
        $statement->bindValue('especialidad', $especialidad);
        $statement->bindValue('salario', $salario);
        // Ejecuto el procedimiento con el numero de registros afectados
        $resultado= $statement->executeStatement();
        
        // Envio a la pagina mandando el numero de registros afectados
        return $this->render('Inicio.html.twig', 
            [
                'afectados' => $resultado,
            ]);
    }

}
