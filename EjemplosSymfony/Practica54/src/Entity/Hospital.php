<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Hospital
 *
 * @ORM\Table(name="hospital")
 * @ORM\Entity
 */
class Hospital
{
    /**
     * @var int
     *
     * @ORM\Column(name="hospital_cod", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $hospitalCod;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=80, nullable=false)
     */
    private $direccion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefono", type="decimal", precision=9, scale=0, nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="num_camas", type="decimal", precision=5, scale=0, nullable=false)
     */
    private $numCamas;

    public function getHospitalCod(): ?int
    {
        return $this->hospitalCod;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(?string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getNumCamas(): ?string
    {
        return $this->numCamas;
    }

    public function setNumCamas(string $numCamas): self
    {
        $this->numCamas = $numCamas;

        return $this;
    }


}
