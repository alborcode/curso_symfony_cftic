<?php

namespace App\Controller;

use App\Entity\Dept;
use App\Repository\DeptRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Id;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PrimerController extends AbstractController
{
    #[Route('/filtros', name: 'ver_datos')]
    public function ver_datos(Request $request, EntityManagerInterface $em)
    { 
        //Cuando trabajamos con DQL, no trabajamos con tablas y campos de las tablas sino con entidades y con propiedades de las entidades.
        // App\Entity\Dept: Namespace del modelo. 
        // Incluimos la ruta completa de la clase Dept.php predeciendolo de su namespace "namespace App\Entity";
        // Obligatorio el alias (u) para poder acceder a las propiedades
        $query = $em->createQuery('SELECT u FROM App\Entity\Dept u WHERE u.id > 1');
        $datos = $query->getResult();

        return $this->render('verDatos.html.twig', 
        [
            'datosDept' => $datos,
        ]);
    }
}
