<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PrimerController extends AbstractController
{
    #[Route('/inicio', name: 'verPagina1')]
    public function verPagina(Request $request)
    {
        // Recupero Identificador de sesion (Token) del usuario de la peticion
        $session = $request->getSession();
        // Guardo variables con ambito sesion, se puede guardar cualquier objeto
        $session->set('nombre', 'Benito');
        $session->set('apellido', 'Floro');
        $session->set('edad', 22);
        $session->set('altura', 1.85);
        // Mandamos a pagina de inicio sin mandar las variables dado que estan en sesion (globales)
        return $this->render('Inicio.html.twig');
    }

    #[Route('/pagina2', name: 'verPagina2')]
    public function verPagina2(Request $request)
    {
        // Recupera variables Session
        $minombre = $request->getSession()->get('nombre');
        $miapellido = $request->getSession()->get('apellido');
        // Ir a pagina VerDatos enviando las variables recogidas en Session
        return $this->render('verDatos.html.twig',
        [
            'nombrevariable'    => $minombre,
            'apellidovariable'  => $miapellido
        ]);
    }

    // Preguntar si existe un dato en la sesión
    // $existe = $session->has('cesta');
    // Usar un valor por defecto si no existe
    // $filtro = $session->get('cesta', []);
    // Destruye la sesión y crea otra nueva.
    // $session->invalidate();
    // Borra todos los atributos de la sesión actual. Permanece la Sesion
    // $session->clear();
}
