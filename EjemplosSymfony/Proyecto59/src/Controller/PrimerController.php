<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// ESPACIOS DE NOMBRES DE LA BIBLIOTECA DOMPDF
use Dompdf\Dompdf;
use Dompdf\Options;

class PrimerController extends AbstractController
{
    #[Route('/Index', name: 'inicio')]
    public function inicio()
    {
        // Existen muchas configuraciones para Dompdf. Incluimos una de las muchas que tiene por ejemplo asignar el tipo de letra
        $pdfOptions = new Options();
        // Tipo de Letra con la que escribir
        $pdfOptions->set('defaultFont', 'Arial');

        // Crea una instancia de Dompdf con nuestras opciones
        $dompdf = new Dompdf($pdfOptions);

        // Preparamos la página HTML para generar PDF generano un objeto renderview
        $html = $this->renderView('figurasPDF.html.twig', 
        [
            'autor' => "Benito Floro"
        ]);

        // Ahora se carga la página HTML en Dompdf
        $dompdf->loadHtml($html);

        // También podemos de forma opcional indicar el tamaño del papel y la orientación
        $dompdf->setPaper('A4', 'portrait');

        // Renderiza el HTML como PDF
        $dompdf->render();

        /* // Envíe el PDF generado al navegador. ¡DESCARGA FORZADA!
         $dompdf->stream("figuras.pdf", [
           "Attachment" => true
        ]); */

        // Podemos generar el pdf y visualizarlo en el navegador si modificamos la propiedad Attachment al valor false.
        $dompdf->stream("figuras.pdf", [
            "Attachment" => false
        ]);

        /* // Almacenar datos binarios PDF
        $output = $dompdf->output();
        // En este caso, queremos escribir el archivo en el directorio público.
        $publicDirectory = $this->getParameter('kernel.project_dir').'/public';
        // Concatenamos el nombre del fichero a la ruta
        $pdfFilepath = $publicDirectory . '/figuras.pdf';
        // Guardamos el archivo en la ruta deseada
        file_put_contents($pdfFilepath, $output);
        // Devolvemos mensaje de texto
        return new Response("¡pdf guardado correctamente!");
 */
    }
}