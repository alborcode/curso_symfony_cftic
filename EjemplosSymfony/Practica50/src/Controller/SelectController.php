<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Emp;
use Doctrine\ORM\EntityManagerInterface;

class SelectController extends AbstractController
{

    // Mostrar Datos Departamentos
    #[Route('/oficios', name: 'formulario')]
    public function formulario(Request $request, EntityManagerInterface $em)
    { 
        return $this->render('empleados.html.twig');
    }

    // Mostrar Datos Departamentos
    #[Route('/empleados', name: 'empleadosOficio')]
    public function empleadosOficio(Request $request, EntityManagerInterface $em)
    {
        // Se recoge con get el parametro que hemos mandado en la caja de texto request al ser post, query si es get
        $datoget = $request->request->get('txtOficio');
        // Select de Emp con Where mandado por parametro (por inyeccion SQL no ponemos $datoget)
        $query = $em->createQuery("SELECT u FROM App\Entity\Emp u WHERE u.oficio = :dato");
        // Asigno valor del parametro dato
        $query->setParameter('dato', $datoget);
        // Al hacer el getresult ejecuta la Query y obtiene los resultados
        $datos = $query->getResult();
        
        return $this->render('empleados.html.twig', 
        [
            'datosEmpleadosOficio' => $datos,
        //    'mensaje' => $mensaje
        ]); 
    }
    
}