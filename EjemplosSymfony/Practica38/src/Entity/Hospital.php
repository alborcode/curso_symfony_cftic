<?php

namespace App\Entity;

use App\Repository\HospitalRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: HospitalRepository::class)]
class Hospital
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $Hospital_cod;

    #[ORM\Column(type: 'string', length: 50)]
    private $Nombre;

    #[ORM\Column(type: 'string', length: 80)]
    private $Direccion;

    #[ORM\Column(type: 'decimal', precision: 9, scale: '0', nullable: true)]
    private $Telefono;

    #[ORM\Column(type: 'decimal', precision: 5, scale: '0')]
    private $num_camas;

    public function getHospital_cod(): ?int
    {
        return $this->Hospital_cod;
    }

    public function getNombre(): ?string
    {
        return $this->Nombre;
    }

    public function setNombre(string $Nombre): self
    {
        $this->Nombre = $Nombre;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->Direccion;
    }

    public function setDireccion(string $Direccion): self
    {
        $this->Direccion = $Direccion;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->Telefono;
    }

    public function setTelefono(?string $Telefono): self
    {
        $this->Telefono = $Telefono;

        return $this;
    }

    public function getNumCamas(): ?string
    {
        return $this->num_camas;
    }

    public function setNumCamas(string $num_camas): self
    {
        $this->num_camas = $num_camas;

        return $this;
    }
}
