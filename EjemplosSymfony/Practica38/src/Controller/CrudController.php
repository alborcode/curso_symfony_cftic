<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Hospital;
use Doctrine\ORM\EntityManagerInterface;

class CrudController extends AbstractController
{

    // Pagina Principal
    #[Route('/', name: 'inicio')]
    public function inicio()
    { 
        return $this->render('inicio.html.twig');
    }

    // Formulario de Alta Hospital
    #[Route('/altaHospital', name: 'nuevoHospital')]
    public function nuevoHospital()
    { 
        return $this->render('alta.html.twig');
    }

    // Proceso de Alta al Dar Submit
    #[Route('/alta', name: 'insertarHospital')]
    public function insertarHospital(Request $request, EntityManagerInterface $em)
    { 
        $nombre = $request->request->get('txtNombre');
        $direccion = $request->request->get('txtDireccion');
        $telefono = $request->request->get('txtTelefono');
        $numcamas = $request->request->get('txtNumeroCamas');
        
        $hospital = new Hospital();
        $hospital->setNombre($nombre);
        $hospital->setDireccion($direccion);
        $hospital->setTelefono($telefono);
        $hospital->setNumCamas($numcamas);
                
        $em->persist($hospital);
        $em->flush();

        // Establece mensaje Flash
        $this->addFlash(
            'notice',
            'Alta realizada Correctamente'
        );
        // Añadir mensajes flash
        //$session->getFlashBag()->add(
        //    'warning',
        //    'Cuidado! Un warning!');

        return $this->redirectToRoute("nuevoHospital");
    }

    // Formulario Baja Hospital
    #[Route('/borrarHospital', name: 'elimHospital')]
    public function elimHospital()
    { 
        return $this->render('eliminar.html.twig');
    }

    // Proceso de Baja al dar Submit
    #[Route('/eliminar', name: 'eliminarHospital')]
        public function eliminarHospital(Request $request, EntityManagerInterface $em)
    { 
        $id = $request->request->get('txtHospitalCod');
        $datos = $em->getRepository(Hospital::class)->find($id);
        // Si no lo encuentra
        if(!$datos) {
            // Establece mensaje Flash
            $this->addFlash(
                'notice',
                'Baja realizada Correctamente'
            );
            return $this->redirectToRoute("elimHospital");
            //$response= new Response();
            //$response->setContent("<div>Codigo de Hospital no existe</div>");
            //return $response;
        }

        $em->remove($datos);
        $em->flush();

        // Establece mensaje Flash
        $this->addFlash(
            'notice',
            'Baja realizada Correctamente'
        );

        return $this->redirectToRoute("elimHospital");

        //$response= new Response();
        //$response->setContent("<div>Dato eliminado correctamente</div>");
        //return $response;
    }

    // Formulario Modificar Hospital
    #[Route('/modificarHospital', name: 'modHospital')]
    public function modHospital()
    { 
        return $this->render('modificar.html.twig');
    }

    #[Route('/modificar', name: 'modificarHospital')]
    public function ModifyDepart(Request $request, EntityManagerInterface $em)
    { 
        $id = $request->request->get('txtHospitalCod');
        $nombre = $request->request->get('txtNombre');
        $direccion = $request->request->get('txtDireccion');
        $telefono = $request->request->get('txtTelefono');
        $numcamas = $request->request->get('txtNumeroCamas');
                
        $hospital = $em->getRepository(Hospital::class)->find($id);
        if(!$hospital) {
            //$response= new Response();
            //$response->setContent("<div>Codigo de Hospital no existe</div>");
            //return $response;
            // Establece mensaje Flash
            $this->addFlash(
                'notice',
                'Codigo de Hospital no existe'
            );

        return $this->redirectToRoute("modHospital");

        }else{
            $hospital->setNombre($nombre);
            $hospital->setDireccion($direccion);
            $hospital->setTelefono($telefono);
            $hospital->setNumCamas($numcamas);
            
            $em->persist($hospital);
            $em->flush();
        }

        // Establece mensaje Flash
        $this->addFlash(
            'notice',
            'Modificacion realizada Correctamente'
        );

        return $this->redirectToRoute("modHospital");

        //$response= new Response();
        //$response->setContent("<div>Hospital modificado correctamente</div>");
        //return $response;

        //return $this->redirectToRoute("modDepart");
    }

    // Mostrar Datos Hospitales
    #[Route('/datosHospitales', name: 'mostrarHospitales')]
    public function mostrarHospitales(Request $request, EntityManagerInterface $em)
    { 
        // Recupero en Array todos los hospitales de la BBDD
        $datos = $em->getRepository(Hospital::class)->findAll();
        return $this->render('hospitales.html.twig', ['datosHospitales' =>$datos]);
    }

}