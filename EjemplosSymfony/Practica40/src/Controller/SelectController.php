<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Dept;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Id;

class SelectController extends AbstractController
{

    // Pagina Principal
    #[Route('/', name: 'inicio')]
    public function inicio()
    { 
        return $this->render('inicio.html.twig');
    }

    // Mostrar Datos Departamentos
    #[Route('/datosDepartamento', name: 'mostrarDepartamentos')]
    public function mostrarDepartamntos(Request $request, EntityManagerInterface $em)
    { 
        // Recupero en Array todos los departamentos de la BBDD
        $datos = $em->getRepository(Dept::class)->findAll();
        return $this->render('departamentos.html.twig', ['datosDepartamentos' => $datos]);
    }

    // Proceso de Busqueda por ID de Departamento. Mismo template
    #[Route('/buscar', name: 'buscarDepartamento')]
    public function buscarDepartamento(Request $request, EntityManagerInterface $em)
    { 
        // Se llama porque la variable es de Peticion no de Sesion, sino apareceria el combo vacio
        $datos = $em->getRepository(Dept::class)->findAll();

        $id = $request->request->get('comboDepartamento');
        // Muestro valor de id
        dump($id);

        $datosDepart = $em->getRepository(Dept::class)->find($id);
        // Muestro valor de registro
        dump($datosDepart);
        return $this->render('departamentos.html.twig', [
           'datosDepartamentos' => $datos,
           'datosDepart' => $datosDepart,
           'seleccionado' => $id
        ]);
    }

}