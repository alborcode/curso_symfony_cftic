<?php

namespace App\Controller;

use App\Entity\Dept;
use App\Repository\DeptRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Id;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProcedimientoController extends AbstractController
{
    #[Route('/inicio', name: 'verPagina')]
    public function verPagina(Request $request, EntityManagerInterface $em)
    { 
        return $this->render('Inicio.html.twig');
    }

    #[Route('/filtros', name: 'eliminarDepart')]
    public function eliminarDepart(Request $request, EntityManagerInterface $em)
    {
        // Recoge el Id en la caja de texto
        $id = $request->request->get('txtId');
        // Obtengo la conexion a la que me he enlazado
        $connection = $em->getConnection();
        // Sobre esa conexion ejecuto el procedimiento con los parametros correspondientes separados por comas
        $statement = $connection->prepare("CALL borrarDept(:dato)");
        // Damos valor a los parametros
        $statement->bindValue('dato', $id);
        // Ejecuto el procedimiento con el numero de registros afectados
        $resultado= $statement->executeStatement();
        
        // Envio a la pagina mandando el numero de registros afectados
        return $this->render('borrado.html.twig', 
            [
                'afectados' => $resultado,
            ]);
    }

}
