<?php

namespace App\Controller;

use App\Entity\Enfermo;
use App\Repository\EnfermoRepository;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Peliculas;
use App\Repository\PeliculasRepository;
use App\Entity\Generos;
use App\Repository\GenerosRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PeliculasController extends AbstractController
{
    // Pagina inicial con los enlaces a las diferentes categorias sin cargar aun peliculas
    #[Route('/inicio', name: 'Inicio')]
    public function Inicio(Request $request, EntityManagerInterface $em)
    {
        // Recupero Identificador de sesion (Token) del usuario de la peticion
        $session = $request->getSession();
        return $this->render('Inicio.html.twig');
    }

    // Pagina para carga dinamica de las peliculas de Accion en Tabla
    #[Route('/accion', name: 'Accion')]
    public function Accion(Request $request, EntityManagerInterface $em)
    {
        // Recupero en Array con Subselect todos las peliculas de la categoria de Accion con DQL
        $query = $em->createQuery(
            "SELECT p 
            FROM App\Entity\Peliculas p
            WHERE p.idgenero =(
                SELECT g.idgenero 
                FROM App\Entity\Generos g 
                WHERE g.genero = 'Accion')"
        );

        dump($query);
        $datosPeliculas = $query->getResult();
        $genero = 'Accion';
        dump($datosPeliculas);
        // Devuelvo a Plantilla los datos recuperados
        return $this->render('Detalle.html.twig', [
            'datosPeliculas' => $datosPeliculas,
            'genero' => $genero,
        ]);
    }

    // Pagina para carga dinamica de las peliculas de Terror en Tabla
    #[Route('/terror', name: 'Terror')]
    public function Terror(Request $request, EntityManagerInterface $em)
    {
        // Recupero en Array con Subselect todos las peliculas de la categoria de Terror con DQL
        $query = $em->createQuery(
            "SELECT p 
            FROM App\Entity\Peliculas p
            WHERE p.idgenero =(
                SELECT g.idgenero 
                FROM App\Entity\Generos g 
                WHERE g.genero = 'Terror')"
        );

        dump($query);
        $datosPeliculas = $query->getResult();
        $genero = 'Terror';
        dump($datosPeliculas);
        // Devuelvo a Plantilla los datos recuperados
        return $this->render('Detalle.html.twig', [
            'datosPeliculas' => $datosPeliculas,
            'genero' => $genero,
        ]);
    }

    // Pagina para carga dinamica de las peliculas de Terror en Tabla
    #[Route('/humor', name: 'Humor')]
    public function Humor(Request $request, EntityManagerInterface $em)
    {
        // Recupero en Array con Subselect todos las peliculas de la categoria de Humor con DQL
        $query = $em->createQuery(
            "SELECT p 
                FROM App\Entity\Peliculas p
                WHERE p.idgenero =(
                    SELECT g.idgenero 
                    FROM App\Entity\Generos g 
                    WHERE g.genero = 'Humor')"
        );

        dump($query);
        $datosPeliculas = $query->getResult();
        $genero = 'Humor';
        dump($datosPeliculas);
        // Devuelvo a Plantilla los datos recuperados
        return $this->render('Detalle.html.twig', [
            'datosPeliculas' => $datosPeliculas,
            'genero' => $genero,
        ]);
    }

    // Pagina para carga dinamica de las peliculas de Terror en Tabla
    #[Route('/romantica', name: 'Romantica')]
    public function Romantica(Request $request, EntityManagerInterface $em)
    {
        // Recupero en Array con Subselect todos las peliculas de la categoria Romantica con DQL
        $query = $em->createQuery(
            "SELECT p 
                FROM App\Entity\Peliculas p
                WHERE p.idgenero =(
                    SELECT g.idgenero 
                    FROM App\Entity\Generos g 
                    WHERE g.genero = 'Romantica')"
        );

        dump($query);
        $datosPeliculas = $query->getResult();
        $genero = 'Romantica';
        dump($datosPeliculas);
        // Devuelvo a Plantilla los datos recuperados
        return $this->render('Detalle.html.twig', [
            'datosPeliculas' => $datosPeliculas,
            'genero' => $genero,
        ]);
    }

    // Pagina en el Path al dar al Checkbox para Almacenar en Session la Pelicula Marcada
    #[Route('/almacenar', name: 'almacenarPelicula')]
    public function almacenarPelicula(
        Request $request,
        EntityManagerInterface $em
    ) {
        // Se recoge con get el Id de la pelicula marcada
        $pelicula = $request->query->get('id');
        dump('$pelicula:' . $pelicula);
        $genero = $request->query->get('genero');
        dump('$genero:' . $genero);

        // Controlamos si existe la variable de Sesion del Array de Peliculas
        $existe = $session->has('arraypeliculas');
        // Pregunto si existe ArrayPeliculas en Session
        if ($existe) {
            // Recupero el Array de Peliculas de Sesion
            $arraypeliculas = $request->getSession()->get('arraypeliculas');
            // Añado la pelicula al array de Peliculas
            array_push($arraypeliculas, $pelicula);
            // Actualizo el array de Peliculas de la Sesion
            $session->set('arraypeliculas', $arraypeliculas);
        } else {
            // Creo Array peliculas vacio
            $arraypeliculas = [];
            // Guardo en Array el empleado
            $arraypeliculas[0] = $pelicula;
            // Guardo Array Empleados en Session
            $session->set('arraypeliculas', $arraypeliculas);
        }

        if ($genero == 'Accion') {
            // Recupero en Array con Subselect todos las peliculas de la categoria Accion con DQL
            $query = $em->createQuery(
                "SELECT p 
                FROM App\Entity\Peliculas p
                WHERE p.idgenero =(
                    SELECT g.idgenero 
                    FROM App\Entity\Generos g 
                    WHERE g.genero = 'Accion')"
            );
            $datosPeliculas = $query->getResult();
        } elseif ($genero == 'Terror') {
            // Recupero en Array con Subselect todos las peliculas de la categoria Terror con DQL
            $query = $em->createQuery(
                "SELECT p 
                FROM App\Entity\Peliculas p
                WHERE p.idgenero =(
                    SELECT g.idgenero 
                    FROM App\Entity\Generos g 
                    WHERE g.genero = 'Terror')"
            );
            $datosPeliculas = $query->getResult();
        } elseif ($genero == 'Humor') {
            // Recupero en Array con Subselect todos las peliculas de la categoria Humor con DQL
            $query = $em->createQuery(
                "SELECT p 
                FROM App\Entity\Peliculas p
                WHERE p.idgenero =(
                    SELECT g.idgenero 
                    FROM App\Entity\Generos g 
                    WHERE g.genero = 'Humor')"
            );
            $datosPeliculas = $query->getResult();
        } elseif ($genero == 'Romantica') {
            // Recupero en Array con Subselect todos las peliculas de la categoria Romantica con DQL
            $query = $em->createQuery(
                "SELECT p 
                FROM App\Entity\Peliculas p
                WHERE p.idgenero =(
                    SELECT g.idgenero 
                    FROM App\Entity\Generos g 
                    WHERE g.genero = 'Romantica')"
            );
            $datosPeliculas = $query->getResult();
        }

        // Recupero en Array todos las Peliculas de la BBDD para enviarlos a div de Pagina Inicial
        $datosempleados = $em->getRepository(Emp::class)->findAll();
        // Vuelvo a mandar a pagina Detalle despues de guardar peliculas marcadas
        return $this->render('Detalle.html.twig', [
            'datosPeliculas' => $datosPeliculas,
            'genero' => $genero,
        ]);
    }

    // Pagina en el Path al dar Ver Carrito
    #[Route('/ver', name: 'verCarrito')]
    public function verCarrito(Request $request, EntityManagerInterface $em)
    {
        // Recupera variable Session arraypeliculas
        $arraypeliculas = $request->getSession()->get('arraypeliculas');
        dump($arraypeliculas);

        $sumaimporte = 0;
        $longitud = sizeof($arraypeliculas);
        dump('$longitud:' . $longitud);

        // Creo Array datos de peluiculas vacio de tipo Emp
        $datospeliculas = [];
        // Recorro el Array de Peliculas para sumar el Precio y sacar Datos de Peliculas Seleccionadas
        for ($i = 0; $i < $longitud; $i++) {
            // Accedo por el ID al precio de la pelicula
            $id = $arraypeliculas[$i];
            $pelicula = $em->getRepository(Peliculas::class)->find($id);
            $datospeliculas[$i] = $pelicula;
            // Recuperamos Precio de la Pelicula
            $precio = $pelicula->getPrecio();
            dump('$precio:' . $precio);
            // Sumo la iteraccion del precio de esa pelicula
            $sumaimporte = $sumaimporte + $precio;
        }

        $mensaje = 'Precio de la compra ' . $sumaimporte;
        dump('$mensaje:' . $mensaje);

        // Mando a Pagina Carrito
        return $this->render('CarritoCompra.html.twig', [
            'datospeliculas' => $datospeliculas,
            'mensaje' => $mensaje,
        ]);
    }

    // Enlace de Quitar Elemento
    #[Route('/eliminar', name: 'quitarPelicula')]
    public function quitarPelicula(Request $request, EntityManagerInterface $em)
    {
        $peliculaborrar = $request->query->get('peliculaborrar');
        dump('$peliculaborrar:' . $peliculaborrar);
        // Recupera variable Session arraypeliculas
        $arraypeliculas = $request->getSession()->get('arraypeliculas');
        dump($arraypeliculas);

        $longitud = sizeof($arraypeliculas);
        dump('$longitud:' . $longitud);

        // Recorro el Array de Peliculas para eliminar de la sesion el ID de la Pelicula que se envia a quitar
        for ($i = 0; $i < $longitud; $i++) {
            if ($arraypeliculas[$i] = $peliculaborrar) {
                // Elimino esa Pelicula del Array de Sesion de Peliculas
                unset($arraypeliculas[$i]);
                $tituloborrado = arraypeliculas[$i]->getTitulo();
                dump('$tituloborrado:' . $tituloborrado);
            }
        }

        // Modificamos el Array de Peliculas de Sesion con el Array de Peliculas Modificado
        $session->set('arraypeliculas', $arraypeliculas);
        $mensaje = 'Eliminada Pelicula ' . $tituloborrado;
        dump('$mensaje:' . $mensaje);

        // Creo Array datos de peliculas vacio de tipo Emp
        $datospeliculas = [];
        $nuevalongitud = sizeof($arraypeliculas);
        // Recorro el Array Actualizado de Peliculas de Sesion para ir guardando los datos de Peliculas a enviar
        for ($i = 0; $i < $nuevalongitud; $i++) {
            // Accedo por el ID a los datos de la Pelicula y guardo en datos a enviar
            $id = $arraypeliculas[$i];
            $pelicula = $em->getRepository(Peliculas::class)->find($id);
            $datospeliculas[$i] = $pelicula;
        }

        // Mando a Pagina Carrito los Datos de las Peliculas Seleccionadas tras el borrado y el mensaje
        return $this->render('ResumenEmpleados.html.twig', [
            'datospeliculas' => $datospeliculas,
            'mensaje' => $mensaje,
        ]);
    }
}
