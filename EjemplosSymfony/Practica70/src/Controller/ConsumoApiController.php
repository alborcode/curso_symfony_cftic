<?php

namespace App\Controller;
use App\Entity\Dept;
use App\Repository\DeptRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ConsumoApiController extends AbstractController
{
    #[Route('/consumoapi', name: 'inicio')]
    public function inicio(Request $request)
    {
        // Usamos file_get_contents para recuperar el contenido
        $datos = file_get_contents("https://datos.madrid.es/egob/catalogo/200342-0-centros-dia.json");
        dump($datos);
        // Decodificamos el Json leido. En el primer argumento la variable en bruto.
        // Cuando es true, los objects JSON devueltos serán convertidos a array asociativos, 
        // Cuando es false los objects JSON devueltos serán convertidos a objects.
        $datosjson = json_decode($datos, true);
        dump($datosjson);

        // En el Array guardo los datos Json de @graph con los registros
        $arrayresidencias = $datosjson['@graph'];
        dump($arrayresidencias);

        return $this->render('residencias.html.twig', [
            'datosResidencias' => $arrayresidencias
        ]);
    }
}