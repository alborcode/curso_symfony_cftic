<?php

namespace App\Controller;
use App\Entity\Dept;
use App\Repository\DeptRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ConsumoApiController extends AbstractController
{
    // Pagina inicial con formulario de Busqueda por Titulo
    #[Route('/inicio', name: 'Inicio')]
    public function Inicio(Request $request, EntityManagerInterface $em)
    { 

        return $this->render('Busqueda.html.twig', []);
    }

    #[Route('/busquedatitulo', name: 'BusquedaTitulo')]
    public function BusquedaTitulo(Request $request)
    {
        // Preparamos URL de API recogemos con GET  
        // $getdato = $request->request->get('txtTitulo');
        $getdato = $request->query->get('txtTitulo');
        dump($getdato);
        $url = 'https://servicioapipeliculas.azurewebsites.net/api/peliculastitulo/'.$getdato;
        // Usamos file_get_contents para recuperar el contenido
        $datos = file_get_contents($url);
        dump($datos);

        // Decodificamos el Json leido. En el primer argumento la variable en bruto.
        // Cuando es true, los objects JSON devueltos serán convertidos a array asociativos, 
        // Cuando es false los objects JSON devueltos serán convertidos a objects.
        $peliculas = json_decode($datos, true);
        dump($peliculas);

        // En el Array guardo los datos Json de @graph con los registros
        //$peliculas = $datosjson['@graph'];
        //dump($arrayresidencias);

        return $this->render('Busqueda.html.twig', [
            'datosPeliculas' => $peliculas
        ]);
    }
}