<?php

namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
class PrimerController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(Request $request)
    {

        return $this->render('index.html.twig', []);
    }

    #[Route('/buscar', name: 'buscarEmpleado')]
    public function buscarEmpleado(Request $request)
    {
        // Recogemos datos de formulario con Get dado que es una busqueda
        // $id = $request->request->get('txtId');
        $id = $request->query->get('txtId');
        dump($id);
        $url = "https://webapibbddhospitales.azurewebsites.net/api/empleados/".$id;
        dump($url);
        $datos = file_get_contents($url);
        $datosId = json_decode($datos, true);
        dump($datosId);

        return $this->render('index.html.twig', [
            'datosId' => $datosId
        ]);
    }

    #[Route('/buscarsalario', name: 'buscarEmpleadosSalario')]
    public function buscarEmpleadosSalario(Request $request)
    {
        // Recogemos datos de formulario con Get dado que es una busqueda
        // $salario = $request->request->get('txtSalario');
        $salario = $request->query->get('txtSalario');
        dump($salario);
        $url = "https://webapibbddhospitales.azurewebsites.net/api/empleados/GetEmpleadosSalario/".$salario;
        $datos = file_get_contents($url);
        $datosEmpleadosSalario = json_decode($datos, true);
        dump($datosEmpleadosSalario);

        return $this->render('index.html.twig', [
            'datosEmpleadosSalario' => $datosEmpleadosSalario
        ]);
    }
    
    // Ruta para Detalle de mostrar Todos los Empleados
    #[Route('/mostrar', name: 'recuperarEmpleados')]
    public function recuperarEmpleados(Request $request)
    { 
        // Recupero todos los empleados de la API
        $datos = file_get_contents("https://webapibbddhospitales.azurewebsites.net/api/empleados");
        $datosEmpleados = json_decode($datos, true);
        dump($datosEmpleados);

        return $this->render('index.html.twig', [
            'datosEmpleados' => $datosEmpleados,
        ]);
    }

    #[Route('/buscaroficiodepartamento', name: 'buscarEmpleadoOficioDepartamento')]
    public function buscarEmpleadoOficioDepartamento(Request $request)
    {
        // Recogemos datos de formulario con Get dado que es una busqueda
        // $oficio = $request->request->get('txtOficio');
        // $departamento = $request->request->get('txtdepartamento');
        $oficio = $request->query->get('txtOficio');
        dump($oficio);
        $departamento = $request->query->get('txtDepartamento');
        dump($departamento);

        $url = "https://webapibbddhospitales.azurewebsites.net/api/empleados/".$oficio."/".$departamento;
        $datos = file_get_contents($url);
        $datosOficioDepartamento = json_decode($datos, true);
        dump($datosOficioDepartamento);

        return $this->render('index.html.twig', [
            'datosOficioDepartamento' => $datosOficioDepartamento
        ]);
    }

}