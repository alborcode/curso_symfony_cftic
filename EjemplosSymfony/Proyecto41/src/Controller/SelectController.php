<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Emp;
use Doctrine\ORM\EntityManagerInterface;

class SelectController extends AbstractController
{

    // Pagina Principal
    #[Route('/', name: 'inicio')]
    public function inicio()
    { 
        return $this->render('inicio.html.twig');
    }

    // Mostrar Datos Departamentos
    #[Route('/seleccionarEmpleado', name: 'mostrarEmpleados')]
    public function mostrarEmpleados(Request $request, EntityManagerInterface $em)
    { 
        // Recupero en Array todos los empleados de la BBDD
        //$datos = strtoupper($em->getRepository(Emp::class)->findAll());  
        //$datoscombo = array_unique($datos);
        //dump($datos);
        //dump($datoscombo);
        $query = $em->createQuery("SELECT DISTINCT (e.oficio) as Oficio FROM App\Entity\Emp e");
        $datos = $query->getResult();

        return $this->render('empleados.html.twig', ['datosEmpleados' => $datos]);
    }

    // Proceso de Busqueda por Oficio de Empleado. Mismo template
    #[Route('/buscar', name: 'buscarEmpleado')]
    public function buscarEmpleado(Request $request, EntityManagerInterface $em)
    { 
        // Se llama porque la variable es de Peticion no de Sesion, sino apareceria el combo vacio
        //$datos = $em->getRepository(Emp::class)->findAll();

        $query = $em->createQuery("SELECT DISTINCT (e.oficio) as Oficio FROM App\Entity\Emp e");
        $datos = $query->getResult();

        $oficio = $request->request->get('comboOficio');
        // Muestro valor de id
        dump($oficio);

        $datosEmpleado = $em->getRepository(Emp::class)->findByOficio($oficio);

                
        return $this->render('empleados.html.twig', [
           'datosEmpleados' => $datos,
           'datosEmpleadosOficio' => $datosEmpleado,
           'seleccionado' => $oficio
        ]);
    }

}