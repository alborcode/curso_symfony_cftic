<?php

namespace App\Controller;
use App\Entity\Dept;
use App\Repository\DeptRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ConsumoApiController extends AbstractController
{
    #[Route('/consumoapi', name: 'inicio')]
    public function inicio(Request $request)
    {
        // https://apiempleadospgs.azurewebsites.net/api/EmpleadosSalario/1234
        // Usamos file_get_contents para recuperar el contenido
        $datos = file_get_contents("https://swapi.dev/api/people/");
        dump($datos);
        // Decodificamos el Json leido. En el primer argumento la variable en bruto.
        // Cuando es true, los objects JSON devueltos serán convertidos a array asociativos, 
        // Cuando es false los objects JSON devueltos serán convertidos a objects.
        $datosjson = json_decode($datos, true);
        dump($datosjson);

        // En el Array guardo los datos Json de Results con los registros
        $arraypersonajes = $datosjson['results'];
        dump($arraypersonajes);

        return $this->render('personajes.html.twig', [
            'datosPersonajes' => $arraypersonajes
        ]);
    }
}