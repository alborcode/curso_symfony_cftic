<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PrimerController extends AbstractController
{
    // Pagina Inicial
    #[Route('/inicio', name: 'Inicio')]
    public function Inicio(Request $request)
    { 

        return $this->render(
            'Pagina1.html.twig'
        );
    }
    
    // Controlador para enviar a pagina de Formulario segun idioma
    #[Route('/{_locale}/idioma', name: 'Idioma')]
    public function Idioma(Request $request, TranslatorInterface $translator)
    { 
        return $this->render(
            'Pagina2.html.twig'
        );

    }

    // Proceso de Alta al Dar Submit
    #[Route('/alta', name: 'altaCarnet')]
    public function insertarAlumnos(Request $request)
    //public function insertarAlumnos(Request $request, EntityManagerInterface $em)
    { 
        $nombre = $request->request->get('txtNombre');
        $apellido1 = $request->request->get('txtApellido1');
        $apellido2 = $request->request->get('txtApellido2');
        $domicilio = $request->request->get('txtDomicilio');
        $email = $request->request->get('txtEmail');
        $confirmalemail = $request->request->get('txtConfirmaEmail');
        // Recuperamos Radios
        $socio = $request->request->get('txtSocio');
        $menor = $request->request->get('txtMenor');
        $residencia = $request->request->get('txtResidencia');
        
        // Aqui iria el Alta del Objeto con las variables recuperadas
        //$carnet = new Carnet();
        //$carnet->setNombre($nombre);
        //$carnet->setApellido1($apellido1);
        //$carnet->setApellido2($apellido2);
        //$carnet->setDomicilio($domicilio);
        //$carnet->setEmail($email);
        //$carnet->setConfirmarEmail($confirmaremail);
        //$carnet->setSocio($socio);
        //$carnet->setMenor($menor);
        //$carnet->setReidencia($residencia);
       
        // Guardo Objeto
        // $em->persist($alumno);
        // Commit en Base Datos
        // $em->flush();

        $mensaje = ' Alta realizada correctamente ';
        return $this->render('Pagina2.html.twig', 
        [
            'mensaje' => $mensaje
        ]);
    }
}