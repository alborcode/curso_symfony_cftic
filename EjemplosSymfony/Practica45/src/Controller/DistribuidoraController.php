<?php

namespace App\Controller;

use App\Entity\Distribuidoras;
use App\Repository\DistribuidoraRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DistribuidoraController extends AbstractController
{
    #[Route('/', name: 'distribuidoras')]
    public function distribuidoras(Request $request, EntityManagerInterface $em)
    { 
        // Recupero en Array todos las distibuidoras de la BBDD
        $datos = $em->getRepository(Distribuidoras::class)->findAll();  
        return $this->render('Distribuidoras.html.twig', ['datosDistribuidoras' => $datos]);
    }

}