<?php

namespace App\Entity;

use App\Repository\DoctorRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DoctorRepository::class)]
class Doctor
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    // El nombre del id se puede cambiar
    private $Doctor_Cod;

    #[ORM\Column(type: 'decimal', precision: 11, scale: '0')]
    private $Hospital_Cod;

    #[ORM\Column(type: 'string', length: 50)]
    private $Apellido;

    #[ORM\Column(type: 'string', length: 20)]
    private $Especialidad;

    #[ORM\Column(type: 'float')]
    private $Salario;

    public function getDoctorCod(): ?int
    {
        return $this->Doctor_Cod;
    }

    public function getHospitalCod(): ?string
    {
        return $this->Hospital_Cod;
    }

    public function setHospitalCod(string $Hospital_Cod): self
    {
        $this->Hospital_Cod = $Hospital_Cod;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->Apellido;
    }

    public function setApellido(string $Apellido): self
    {
        $this->Apellido = $Apellido;

        return $this;
    }

    public function getEspecialidad(): ?string
    {
        return $this->Especialidad;
    }

    public function setEspecialidad(string $Especialidad): self
    {
        $this->Especialidad = $Especialidad;

        return $this;
    }

    public function getSalario(): ?string
    {
        return $this->Salario;
    }

    public function setSalario(string $Salario): self
    {
        $this->Salario = $Salario;

        return $this;
    }

}
