<?php

namespace App\Controller;

use App\Entity\Dept;
use App\Repository\DeptRepository;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PrimerController extends AbstractController
{
    #[Route('/borrarDepart', name: 'elimDepart')]
    public function elimDepart()
    { 
        return $this->render('eliminar.html.twig');
    }

    #[Route('/eliminar', name: 'eliminarDepart')]
    // Con Request recojo valores de Formulario
    public function borrarDepart(Request $request, EntityManagerInterface $em)
    { 
        $identificador = $request->request->get('txtId');
        // recupero getRepository y nombre de clase puedo acceder a todos los metodos de ese repositorio
        // Funcion find y paso el identificador, datos tendra la informacion de la fila
        $datos = $em->getRepository(Dept::class)->find($identificador);
        // Si no lo encuentra
        if(!$datos) {
            return $this->render('noEncontrado.html.twig', [
            'mensaje' => 'Departamento no existe'
            ]);
        }
        // Borrado enviando variable $datos de tipo Dept
        $em->remove($datos);
        // Commit
        $em->flush();
            return $this->render('ok.html.twig', [
                'mensaje' => 'Dato eliminado correctamente:'. $identificador
            ]);
    }
}