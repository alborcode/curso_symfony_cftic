<?php

namespace App\Controller;

use App\Entity\Dept;
use App\Repository\DeptRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Id;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ModificarController extends AbstractController
{
    #[Route('/modifiDepart', name: 'modDepart')]
    public function modDepart()
    { 
        return $this->render('modificar.html.twig');
    }

    #[Route('/modificar', name: 'ModifyDepart')]
    public function ModifyDepart(Request $request, EntityManagerInterface $em)
    { 
        // Podemos obtener el EntityManager a través de inyección de dependencias con el argumento EntityManagerInterface $em
        // 1) recibir datos del formulario
        $identificador = $request->request->get('txtId');
        $nombre = $request->request->get('txtNombre');
        $loc = $request->request->get('txtLoc');
        
        $departamento = $em->getRepository(Dept::class)->find($identificador);
        if(!$departamento) {
            echo 'Departamento no existe';
        }else{
            $departamento->setDnombre($nombre);
            $departamento->setLoc($loc);
            // Modifico el objeto
            $em->persist($departamento);
            // Para ejecutar las queries pendientes, se utiliza flush().
            $em->flush();
        }    
        return $this->redirectToRoute("modDepart");
    }

}