<?php

namespace App\Controller;

use App\Entity\Dept;
use App\Repository\DeptRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DepartamentosController extends AbstractController
{
    #[Route('/inicio', name: 'Inicio')]
    public function hospitales(Request $request, EntityManagerInterface $em)
    { 
        // Recupero en Array todos los departamentos de la BBDD y se envian a Plantilla de Inicio
        $datos = $em->getRepository(Dept::class)->findAll();  
        return $this->render('Departamentos.html.twig', 
        [
            'datosDepartamentos' => $datos
        ]);
    }

    // Proceso al dar Borrar en enlace de cada departamento
    #[Route('/eliminar', name: 'borrarDepartamento')]
        public function borrarDepartamento(Request $request, EntityManagerInterface $em)
    { 
        // Se recoge con get (query->get) no por post (request->get) los datos del departamento a modificar
        //$id = $request->request->get('numdpto');
        $id=$request->query->get("numdpto");
        dump('$id:'.$id);

        $departamentoborrar = $em->getRepository(Dept::class)->find($id);
        // Se valida si existe el departamento a Borrar
        if(!$departamentoborrar) {
            // Construimos mensaje de error
            $mensaje = 'Error al eliminar departamnto '.$id;
        }else{
            $em->remove($departamentoborrar);
            $em->flush();
            // Construimos mensaje de baja departamento
            $mensaje = 'Se ha eliminado departamento '.$id;
        }

        // Recuperamos de nuevo todos los departamentos existentes tras borrado
        $datos = $em->getRepository(Dept::class)->findAll();  

        // Enviamos de nuevo a Pagina Inicial los departamentos existentes y el mensaje
        return $this->render('Departamentos.html.twig', 
        [
            'datosDepartamentos'    => $datos,
            'mensaje'               => $mensaje
        ]);
    }

    // Formulario Modificar Hospital al que se enviará el numero de Departamento a Modificar
    #[Route('/formulariomodificar', name: 'editarDepartamento')]
    public function editarDepartamento(Request $request, EntityManagerInterface $em)
    { 
        // Se recoge con get (query->get) no por post (request->get) los datos del departamento a modificar
        //$id = $request->request->get('numdpto');
        $id=$request->query->get("numdpto");
        $dnombre=$request->query->get("dnombre");
        $loc=$request->query->get("loc");
        dump('$id:'.$id);
        dump('$nombre:'.$dnombre);
        dump('$loc:'.$loc);

        // Llamamos Pantilla de Modificacion, mandando el registro a modificar
        return $this->render('ModificarDepartamento.html.twig', 
        [
            'id'        => $id,
            'dnombre'   => $dnombre,
            'loc'       => $loc
        ]);
    }

    // Proceso al dar al Boton Editar despues de rellenar formulario de modificacion de Departamento
    #[Route('/modificar', name: 'modificarDepartamento')]
    public function modificarDepartamento(Request $request, EntityManagerInterface $em)
    { 
        // Se recogen los datos enviados del Formulario de Modificacion
        $id = $request->request->get('txtDeptNo');
        $nombre = $request->request->get('txtNombre');
        $localizacion = $request->request->get('txtLocalizacion');
        dump('$id:'.$id);
        dump('$nombre:'.$nombre);
        dump('$loc:'.$localizacion);

        // Control de que exista el registro a modificar
        $departamentomodificar = $em->getRepository(Dept::class)->find($id);

        if(!$departamentomodificar) {
            // Construimos mensaje de error
            $mensaje = 'Error al modificar departamento '.$id;
        // Se realiza modificacion de registro        
        }else{
            // Modificamos los valores con los datos del Formulario, el ID no se puede modificar es clave
            // $departamentomodificar->setId($id);
            $departamentomodificar->setDNombre($nombre);
            $departamentomodificar->setLoc($localizacion);
            $em->persist($departamentomodificar);
            $em->flush();
            // Construimos mensaje de modificacion departamento
            $mensaje = 'Se ha modificado el departamento '.$id;
        }

        // Recuperamos de nuevo todos los departamentos existentes tras modificacion
        $datos = $em->getRepository(Dept::class)->findAll();  

        // Enviamos de nuevo a Pagina Inicial los departamentos existentes y el mensaje
        return $this->render('Departamentos.html.twig', 
        [
            'datosDepartamentos'    => $datos,
            'mensaje'               => $mensaje
        ]);
    }

    // Formulario Alta Departamento al que se llama desde Enlace
    #[Route('/formularioalta', name: 'nuevoDepartamento')]
    public function nuevoDepartamento(Request $request, EntityManagerInterface $em)
    { 
        // Llamamos Pantilla de Alta
        return $this->render('AltaDepartamento.html.twig');
    }

    // Proceso al dar el Alta despues de rellenar formulario de alta de Departamento
    #[Route('/alta', name: 'insertarDepartamento')]
    public function insertarDepartamento(Request $request, EntityManagerInterface $em)
    { 
        // Se recogen los datos enviados del Formulario de Modificacion
        $id = $request->request->get('txtDeptNo');
        $nombre = $request->request->get('txtNombre');
        $localizacion = $request->request->get('txtLocalizacion');

        $departamentoalta = new Dept();
        // El numero de departamento se asigna automaticamente al ser autoincremental
        // $departamentoalta->setId($id);
        $departamentoalta->setDNombre($nombre);
        $departamentoalta->setLoc($localizacion);
                
        $em->persist($departamentoalta);
        $em->flush();

        // Construimos mensaje de alta departamento
        $mensaje = 'Se ha dado de alta el departamento ';

         // Recuperamos de nuevo todos los departamentos existentes tras alta
         $datos = $em->getRepository(Dept::class)->findAll();  

         // Enviamos de nuevo a Pagina Inicial los departamentos existentes y el mensaje
         return $this->render('Departamentos.html.twig', 
         [
             'datosDepartamentos'    => $datos,
             'mensaje'               => $mensaje
         ]);
    }
    
}