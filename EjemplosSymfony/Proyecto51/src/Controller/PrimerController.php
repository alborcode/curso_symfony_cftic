<?php

namespace App\Controller;

use App\Entity\Dept;
use App\Repository\DeptRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Id;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PrimerController extends AbstractController
{
    #[Route('/filtros', name: 'ver_datos')]
    public function ver_datos(Request $request, EntityManagerInterface $em)
    { 
        $query = $em->createQuery('SELECT u FROM App\Entity\Dept u WHERE u.loc like :dato');
        $query->setParameter('dato', "B%");
        $datos = $query->getResult();
        
        return $this->render('verDatos.html.twig', 
        [
            'datosDept' => $datos,
        ]);
    }

}
