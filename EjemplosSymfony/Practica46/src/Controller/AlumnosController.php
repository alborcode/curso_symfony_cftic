<?php

namespace App\Controller;

use App\Entity\Alumnos;
use App\Repository\DistribuidoraRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AlumnosController extends AbstractController
{

    // Formulario de Alta de Clientes
    #[Route('/cliente', name: 'formularioAlta')]
    public function formularioAlta()
    {
        return $this->render('Clientes.html.twig');
    }

    // Proceso de Alta al Dar Submit
    #[Route('/alta', name: 'insertarAlumnos')]
    public function insertarAlumnos(Request $request, EntityManagerInterface $em)
    { 
        $nombre = $request->request->get('txtNombre');
        $apellido1 = $request->request->get('txtApellido1');
        $apellido2 = $request->request->get('txtApellido2');
        $domicilio = $request->request->get('txtDomicilio');
        $ciudad = $request->request->get('comboCiudad');
        $sexo = $request->request->get('txtSexo');
        // Recogemos Array de Sistemas Operativos Seleccionados
        $checkbox = $request->request->all('txtSO');
        $comentario = $request->request->get('txtComentarios');
        
        $alumno = new Alumnos();
        $alumno->setNombre($nombre);
        $alumno->setApellido1($apellido1);
        $alumno->setApellido2($apellido2);
        $alumno->setDomicilio($domicilio);
        $alumno->setCiudad($ciudad);
        $alumno->setSexo($sexo);
        $alumno->setComentarios($comentario);

        $sistemasoperativos="";
        // Concatenamos en la variable sistemasoperativos todos los valores que nos llegan checkeados añadiendo ,
        foreach ($checkbox as $valor) {
            $sistemasoperativos.= $valor.",";
        }
        // Eliminamos la última coma
        $resultadosincoma=substr($sistemasoperativos, 0, strlen($sistemasoperativos)-1); 
        // Añado el valor al campo a Insertar
        $alumno->setSistemaoperativo($resultadosincoma);
        
        // Guardo Objeto
        $em->persist($alumno);
        // Commit en Base Datos
        $em->flush();

        return $this->render('Clientes.html.twig', 
        [
            'mensaje' => 'Sr'.' '.$apellido1.' '.$apellido2.' '.'acaba de almacenar sus datos en nuestra base de datos.'
        ]);
    }

}
