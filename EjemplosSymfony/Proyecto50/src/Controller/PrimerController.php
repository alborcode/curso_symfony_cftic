<?php

namespace App\Controller;

use App\Entity\Dept;
use App\Repository\DeptRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PrimerController extends AbstractController
{
    #[Route('/inicio', name: 'home')]
    public function home()
    { 
        return $this->render('Inicio.html.twig');
    }

    #[Route('/filtros', name: 'ver_datos')]
    public function ver_datos(Request $request, EntityManagerInterface $em)
    { 
        // Se recoge con get el parametro que hemos mandado
        $datoget = $request->query->get('id');
        // Select de Dept con Where mandado por parametro (por inyeccion SQL no ponemos $datoget)
        // El parametro debe ir precedido de :
        $query = $em->createQuery("SELECT u FROM App\Entity\Dept u WHERE u.id = :dato");
        // Asigno valor del parametro dato
        $query->setParameter('dato', $datoget);
        // Al hacer el getresult ejecuta la Query y obtiene los resultados
        $datos = $query->getResult();

        // Si no existe datos es que el departamento no existe
        if(!$datos) {
            return $this->render('VerDatos.html.twig', [
                'mensaje' => 'Departamento no existe'
            ]);
        }else{
            return $this->render('VerDatos.html.twig', [
                'datosDept' => $datos,
            ]);
        }
    }
}