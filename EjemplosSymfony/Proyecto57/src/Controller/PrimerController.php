<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PrimerController extends AbstractController
{
    #[Route('/Index', name: 'inicio')]
    public function inicio()
    {
        return $this->render('Index.html.twig');
    }

    #[Route('/RecuperarGet', name: 'Recuperar')]
    public function personas(Request $request)
    {
        // Recojo con Query al ser GET los datos que nos envian
        $datoget = $request->query->get('id');
        $datoget2 = $request->query->get('nombre');
        
        // Muestro verdatos mandando los parametros
        return $this->render('verdatos.html.twig', [
            'a' => $datoget,
            'b' => $datoget2
        ]);
    }
}