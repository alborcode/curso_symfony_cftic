<?php

namespace App\Controller;

use App\Entity\Dept;
use App\Repository\DeptRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PrimerController extends AbstractController
{
    #[Route('/inicio', name: 'home')]
    public function home()
    { 
        return $this->render('Inicio.html.twig');
    }

    #[Route('/create', name: 'ver_datos')]
    public function ver_datos(Request $request, EntityManagerInterface $em)
    { 
        // Se recoge con get el parametro que hemos mandado
        $datoget = $request->query->get('id');
        // Busco por el Id que me llega
        $datos = $em->getRepository(Dept::class)->findById($datoget);
        // Si no existe datos es que el departamento no existe
        if(!$datos) {
            return $this->render('VerDatos.html.twig', [
                'mensaje' => 'Departamento no existe'
            ]);
        }else{
            return $this->render('VerDatos.html.twig', [
                'datosDept' => $datos,
            ]);
        }
    }
}