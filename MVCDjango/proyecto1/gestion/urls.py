from django.urls import path

from gestion import views

# Va a la funcion index de views si no se pone nada
urlpatterns = [
    path('', views.index, name='index'),
    path('deportes', views.index, name='index2'),
    path('marketing', views.index, name='index3'),
    path('ventas', views.index, name='index4')
]
