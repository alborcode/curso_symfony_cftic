<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title></title>
    
    </head>
    <body>

        <?php
            // Comprueba si esta definido el Array de Frutas con isset para ver si existe la variable
            // el nombre del array se recoge sin corchetes
            if(isset($_POST["fruta"])){
                // $numero es el array de frutas
                $numero=$_POST["fruta"];
                // $count es la longitud del array
                $count = count($numero);
                // Recorremos del primer elemento al ultimo
                for ($i = 0; $i < $count; $i++) {
                    echo "<div style='background-color:fuchsia'>";
                    // Imprimimos el elemento del array
                    echo "Elementos seleccionados: ".$numero[$i]."</div>";
                }
            }
            // Controlamos Array por si no se ha marcado nada
            else{
                echo "SIN SELECCIÓN";
            }
        ?>

    </body>
</html>
