<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title></title>
    
    </head>
    <body>
        <p> <?php echo htmlspecialchars('Nombre:' . $_POST['nombre']); ?> </p>
        <p> <?php echo htmlspecialchars('Primer Apellido:' . $_POST['apellido1']); ?> </p>
        <p> <?php echo htmlspecialchars('Segundo Apellido:' . $_POST['apellido2']); ?> </p>
        <p> <?php echo htmlspecialchars('Domicilio:' . $_POST['domicilio']); ?> </p>
        <p> <?php echo htmlspecialchars('Ciudad:' . $_POST['comboCiudad']); ?> </p>
        <p> <?php echo htmlspecialchars('Sexo:' . $_POST['sexo']); ?> </p>
        <p>
        <?php
            // Comprueba si esta definido el Array de Sistemas Operativos            
            if(isset($_POST["sistema"])){
                $sistemas = $_POST["sistema"];
                $longitud = count($sistemas);
                for ($i = 0; $i < $longitud; $i++) {   
                    // Cargo Variable a mostrar
                    $texto_sistema = "Sistema Operativo: ".$sistemas[$i].",";
                    // Si es la ultima ocurrencia escribo quitando , con substr
                    if ($i == ($longitud - 1) ){
                        // $string[strlen($texto-1)] ultimo caracter de cadena
                        echo substr( $texto_sistema, 0, (strlen($texto_sistema)-1) );
                    }
                    else{
                        echo $texto_sistema;
                    }
                    // Con If tambien se puede hacer
                    /* if ($i == ($longitud - 1) ){
                        echo "Sistema Operativo: ".$sistemas[$i];
                    }
                    else{
                        echo "Sistema Operativo: ".$sistemas[$i].",";
                    }  */
                }
            }
            else{
                echo "Sin Seleccion de Sistema";
            }
        ?>
        </p>
        <p> <?php echo htmlspecialchars('Comentarios:' . $_POST['comentarios']); ?> </p>
        
    </body>
</html>
