﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiBBDD.Models;
using WebApiBBDD.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApiBBDD.Controllers
{
    // Ruta api/empleados (controller es el nombre de la clase Controller, sin el Controller final)
    [Route("api/[controller]")]
    [ApiController]
    public class EmpleadosController : ControllerBase
    {
        RepositoryEmpleados repo;

        public EmpleadosController(RepositoryEmpleados repo)
        {
            this.repo = repo;
        }

        // Acceso con get a https://webapibbddhospitales.azurewebsites.net/api/empleados
        // Devuelve todos los empleados
        [HttpGet]
        public ActionResult<List<Empleado>> Get()
        {
            return this.repo.GetEmpleados();
        }

        // Acceso con get y parametro id a https://webapibbddhospitales.azurewebsites.net/api/empleados/7839
        // Buscar por id, devuelve el empleado con ese id
        [HttpGet("{id}")]
        public ActionResult<Empleado> Get(int id)
        {
            return this.repo.BuscarEmpleado(id);
        }

        // Acceso con get y parametro id a https://webapibbddhospitales.azurewebsites.net/api/empleados/analista/1
        // Buscar por oficio y departamento, devuelve los empleados
        [HttpGet("{oficio}/{departamento}")]
        public ActionResult<List<Empleado>> GetEmpleados(String oficio, int departamento)
        {
            return this.repo.GetEmpleados(oficio, departamento);
        }

        // Acceso con get y parametro id a https://webapibbddhospitales.azurewebsites.net/api/empleados/GetEmpleadosSalario/318500
        // Buscar empleados con un salario igual o mayor
        [HttpGet("[action]/{salario}")]
        public ActionResult<List<Empleado>> GetEmpleadosSalario(int salario)
        {
            return this.repo.GetEmpleadosSalario(salario);
        }
    }
}