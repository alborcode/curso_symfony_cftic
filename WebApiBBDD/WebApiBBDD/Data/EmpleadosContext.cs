﻿using WebApiBBDD.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiBBDD.Data
{
    public class EmpleadosContext : DbContext
    {
        public EmpleadosContext(DbContextOptions options) : base(options)
        { }

        // Propiedad Empleados del tipo Clase de la que vamos a serializar
        public DbSet<Empleado> Empleados { get; set; }
    }
}
