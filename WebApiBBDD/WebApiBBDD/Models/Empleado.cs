﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiBBDD.Models
{
    // Deserializa la Tabla EMP en la clase Empleado
    [Table("EMP")]
    public class Empleado
    {
        // Key es la clave principal
        [Key]
        [Column("EMP_NO")]
        public int IdEmpleado { get; set; }
        // Deserializa la columna apellido en la propiedad Apellido
        [Column("APELLIDO")]
        public String Apellido { get; set; }

        [Column("OFICIO")]
        public String Oficio { get; set; }

        [Column("SALARIO")]
        public int Salario { get; set; }

        [Column("DEPT_NO")]
        public int Departamento { get; set; }
    }

}
