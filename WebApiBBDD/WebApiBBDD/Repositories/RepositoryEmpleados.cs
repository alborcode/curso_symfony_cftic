﻿using WebApiBBDD.Data;
using WebApiBBDD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiBBDD.Repositories
{
    // Repositorio de Entidad Empleados Metodos de Acceso a Entidad Empleados
    public class RepositoryEmpleados
    {
        EmpleadosContext context;

        public RepositoryEmpleados(EmpleadosContext context)
        {
            this.context = context;
        }

        // Metodo para devolver los datos de todos los Empleados
        public List<Empleado> GetEmpleados()
        {
            return this.context.Empleados.ToList();
        }

        // Metodo para devolver un empleado con un id
        public Empleado BuscarEmpleado(int idempleado)
        {
            return this.context.Empleados.SingleOrDefault(z => z.IdEmpleado == idempleado);
        }

        // Metodo para devolver empleados con salario mayor o igual
        public List<Empleado> GetEmpleadosSalario(int salario)
        {
            return this.context.Empleados.Where(x => x.Salario >= salario).ToList();
        }

        // Metodo para devolver empleados con un oficio de un departmento
        public List<Empleado> GetEmpleados(String oficio, int departamento)
        {
            var consulta = from datos in context.Empleados
                           where datos.Oficio == oficio
                           && datos.Departamento == departamento
                           select datos;
            return consulta.ToList();
        }
    }
}