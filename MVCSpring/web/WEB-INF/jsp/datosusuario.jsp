<%-- 
    Document   : datosusuario
    Created on : 09-sep-2022, 13:07:39
    Author     : Mañanas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- Cargo Etiquetas JSTL para poder usarlas --%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="estilotabla.css"/>
    </head>
    <body>
        <h1>VISTA USUARIO</h1>
        <%--  Cargo con jstl:out el valor de mensaje --%>
        <h4><jstl:out value="${mensaje}"/></h4>
        <table border="1">
            <tr>
                <th>Nombre</th>
                <td>
                    <jstl:out value="${usuario.nombre}"/>
                </td>
            </tr>            
            <tr>
                <th>Apellidos</th>
                <td>
                    <jstl:out value="${usuario.apellidos}"/>
                </td>
            </tr>
            <tr>
                <th>Edad</th>
                <td>
                    <jstl:out value="${usuario.edad}"/>
                </td>
            </tr>            
        </table>
    </body>
</html>


