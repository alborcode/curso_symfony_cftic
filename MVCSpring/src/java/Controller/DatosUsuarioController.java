/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Models.Usuario;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 *
 * @author Mañanas
 */
public class DatosUsuarioController implements Controller {
     @Override
    public ModelAndView handleRequest(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
        // Definimos objeto ModelAndViewm el nombre es el de la pagina JSP
        ModelAndView mv = new ModelAndView("datosusuario");
        
        String mensaje= "Datos de Usuario de la VISTA:";
        
        Usuario usu = new Usuario();
        usu.setNombre("Lucia");
        usu.setApellidos("Ramos");
        usu.setEdad(24);
        
        // Añado el objeto mensaje al que le envio la variable mensaje
        mv.addObject("mensaje", mensaje);
        // Añado el objeto usuario al que le envio la variable usu
        mv.addObject("usuario",usu);
        // Devuelvo vista
        return mv;
    }
}
