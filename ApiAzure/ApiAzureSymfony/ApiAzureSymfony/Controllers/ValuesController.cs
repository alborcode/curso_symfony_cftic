﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ApiAzureSymfony.Controllers
{
    // La ruta será api y nombre de controlador
    [Route("api/[controller]")]
    [ApiController]
    // los : son el extend de PHP
    public class ValuesController : ControllerBase
    {
        // Si se hace peticion GET https://localhost:44306/api/values
        // https://apiazuresymfony20220906102006.azurewebsites.net/api/values

        List<Persona> listapersonas = new List<Persona>();

        [HttpGet]
        // Metodo definimos que devuelve string. Si no devuelve nada void
        public List<Persona> mensaje() {
            // Añado cuatro objetos de Tipo Personas
            Persona p = new Persona { IdPersona = 1, Nombre = "Lucia", Email = "lucia@gmail.com", Edad = 19 };
            this.listapersonas.Add(p);
            p = new Persona { IdPersona = 2, Nombre = "Adrian", Email = "adrian@gmail.com", Edad = 24 };
            this.listapersonas.Add(p);
            p = new Persona { IdPersona = 3, Nombre = "Alejandro", Email = "alejandro@gmail.com", Edad = 21 };
            this.listapersonas.Add(p);
            p = new Persona { IdPersona = 4, Nombre = "Sara", Email = "sara@gmail.com", Edad = 17 };
            this.listapersonas.Add(p);
            return this.listapersonas;
        }

        // Si se hace peticion GET con un parametro https://localhost:44306/api/values/48
        // https://apiazuresymfony20220906102006.azurewebsites.net/api/values/48
        [HttpGet("{edad}")]
        public string mensaje2(int edad)
        {
            // Concateno mensaje con parametro
            return "Alberto, tienes "+ edad +" años";
        }

        // Si se hace peticion GET con Action https://localhost:44306/api/values/pedro/sanchez
        // https://apiazuresymfony20220906102006.azurewebsites.net/api/values/pedro/sanchez
        [HttpGet("{nombre}/{apellido}")]
        public string mensaje3(string nombre, string apellido)
        {
            // Concateno mensaje con parametro
            return "nombre: " + nombre +  " apellido: " + apellido;
        }

        // Si se hace peticion GET con Action entro con nombre metodo mas parametro https://localhost:44306/api/values/ciudad/madrid
        // https://apiazuresymfony20220906102006.azurewebsites.net/api/values/ciudad/madrid
        [HttpGet("[action]/{nombre}")]
        public string ciudad(string nombre)
        {
            // Concateno mensaje con parametro
            return "ciudad: " +nombre;
        }

    }   
}
