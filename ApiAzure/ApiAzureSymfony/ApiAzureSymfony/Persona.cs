﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiAzureSymfony
{
    public class Persona
    {
        // Propiedades de Lectura (get) y escritura (set)
        public int IdPersona { get; set; }
        public String Nombre { get; set; }
        public String Email { get; set; }
        public int Edad { get; set; }
    }
}
