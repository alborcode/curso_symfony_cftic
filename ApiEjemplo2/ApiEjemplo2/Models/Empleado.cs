﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEjemplo2.Models
{
    public class Empleado
    {
        // poniendo prop y dos tabuladores genera automaticamente
        public int id { get; set; }
        // string en minuscula es tipo dato, String en mayuscula es clase que tendra metodos de cadenas
        public String apellido { get; set; }
        public String oficio { get; set; }
        public int salario { get; set; }
    }
}
