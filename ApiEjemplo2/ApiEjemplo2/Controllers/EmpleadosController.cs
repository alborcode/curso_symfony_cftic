﻿using ApiEjemplo2.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiEjemplo2.Controllers
{
    // Ruta api/Empleados
    [Route("api/[controller]")]
    [ApiController]
    public class EmpleadosController : ControllerBase
    {
        // Declaro a la altura de la clase para que sea accesible en todos los metodos
        // Declaro una coleccion de tipo Empleados (los Arrays no se pueden redimensionar, las colecciones si)
        List<Empleado> listaEmpleados = new List<Empleado>();

        // Metodo Constructor, tendra el mismo nombre de la clase, Segun se instancie se ejecutara este metodo
        public EmpleadosController()
        {
            // Creamos objeto empleado. String comillas dobles char comilla simple. Forma tradicional
            // Empleado e1 = new Empleado();
            // e1.id = 1;
            // e1.apellido = "Alberto";
            // e1.oficio = "Programador";
            //e1.salario = 12345;
            //listaEmpleados.Add(e1);

            // Forma moderna de hacerlo
            Empleado e1 = new Empleado
            {
                id = 1,
                apellido = "Alberto",
                oficio = "Programador",
                salario = 12345
            };
            listaEmpleados.Add(e1);

            Empleado e2 = new Empleado
            {
                id = 2,
                apellido = "Fernando",
                oficio = "Programador",
                salario = 4567
            };
            listaEmpleados.Add(e2);

            Empleado e3 = new Empleado
            {
                id = 3,
                apellido = "Antonio",
                oficio = "Analista",
                salario = 53345
            };
            listaEmpleados.Add(e3);

            Empleado e4 = new Empleado
            {
                id = 4,
                apellido = "Alejandro",
                oficio = "Analista",
                salario = 62345
            };
            listaEmpleados.Add(e4);

            Empleado e5 = new Empleado
            {
                id = 5,
                apellido = "Angel",
                oficio = "Jefe",
                salario = 912345
            };
            listaEmpleados.Add(e5);
        }

        // GET: api/Empleados
        [HttpGet]
        // Devuelve una lista de Empleados
        public List<Empleado> devolverDatos()
        {
            return listaEmpleados;
        }

        // GET: api/Empleados/{id}
        [HttpGet("{id}")]
        // Devuelve un solo Empleado
        public Empleado devolverEmpleado(int id)
        {
            // con Linq busco con expresion Lambda en el Array aquel registro cuyo id sea el pasado
            Empleado e = listaEmpleados.Find(z => z.id == id);
            return e;
        }

        // GET: api/Empleados/devolverOficio/{oficio}
        [HttpGet("[action]/{oficio}")]
        // Devuelve un solo Empleado
        public List<Empleado> devolverOficio(string oficio)
        {
            // con Linq busco con expresion Lambda en el Array con where devuelve una lista aquel registro cuyo id sea el pasado
            List<Empleado> eoficio = listaEmpleados.Where(z => z.oficio == oficio).ToList();
            return eoficio;
        }


    }
}
